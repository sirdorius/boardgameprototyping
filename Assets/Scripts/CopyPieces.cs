﻿using UnityEngine;

public class CopyPieces : MonoBehaviour {
	public Transform source;
	public SquareGridDirection orientation;

	void Awake() {
		foreach (Transform t in source) {
			var go = Instantiate(t.gameObject) as GameObject;
			go.transform.parent = transform;
			go.transform.localPosition = t.localPosition;
			go.transform.localRotation = t.localRotation;
		}

		foreach (Piece p in GetComponentsInChildren<Piece>())
			p.orientation = orientation;
	}
}