﻿using UnityEngine;
using System.Collections.Generic;

public abstract class MovementDescriptor : MonoBehaviour {

	[System.Serializable]
	public struct ValidPosition {
		public IntVector2 pos;
		public GridTile.HighlightType type;
	}

	protected Piece piece;
	protected void Awake() {
		piece = GetComponent<Piece>();
	}

	public abstract List<ValidPosition> validPositions(IntVector2 current);
}