﻿using UnityEngine;
using System.Collections.Generic;

public class SquareGridMovementDescriptor : MovementDescriptor {
	[System.Serializable]
	public struct DirectionDescriptor {
		public SquareGridDirection direction;
		public bool use;
	}

	[System.Serializable]
	public struct ListOfSquareGridDirection {
		public GridTile.HighlightType type;
		public List<DirectionDescriptor> directions;
		public int iterationDepth;
	}

	public List<ListOfSquareGridDirection> movement;

	public override List<ValidPosition> validPositions(IntVector2 current) {
		var initCurrent = current;
		var finalPositions = new List<ValidPosition>();
		foreach (var possibleMovement in movement) {
			for (int i = 0; i < possibleMovement.iterationDepth+1; i++) {
				IntVector2 temp = new IntVector2(0, 0);
				foreach (var intermediateStep in possibleMovement.directions) {
					temp += (adaptMovementToOrientation(SquareGrid.dirToVect(intermediateStep.direction)));
					if (intermediateStep.use) {
						finalPositions.Add(new ValidPosition { pos = current + temp, type = possibleMovement.type });
					}
				}
				current += temp;
			}
			current = initCurrent;
		}
		return finalPositions;
	}

	protected IntVector2 adaptMovementToOrientation(IntVector2 v) {
		if (piece.orientation == SquareGridDirection.North)
			return v;
		else if (piece.orientation == SquareGridDirection.West) {
			var t = v.x;
			v.x = -v.y;
			v.y = t;
			return v;
		}
		else if (piece.orientation == SquareGridDirection.South) {
			v.x = -v.x;
			v.y = -v.y;
			return v;
		}
		else if (piece.orientation == SquareGridDirection.East) {
			var t = v.x;
			v.x = v.y;
			v.y = -t;
			return v;
		}
		else
			throw new UnityException("Rotation on GameObject " + gameObject.name + " is invalid (must be 0/90/180/270, but was " + transform.eulerAngles.z + ")");
	}
}