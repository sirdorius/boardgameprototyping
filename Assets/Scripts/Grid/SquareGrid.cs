﻿using UnityEngine;
using System.Collections.Generic;

public enum SquareGridDirection { North, East, South, West, NorthEast, NorthWest, SouthEast, SouthWest };

// TODO Distinguish between hax grid and block grid
public class SquareGrid : Grid {

	#region Grid construction
	List<GridTile> tempTiles = new List<GridTile>();

	public override void registerGridTile(GridTile tile) {
		tempTiles.Add(tile);
		updateGridMinMax(tile, tile.transform.position.x, tile.transform.position.y);
	}
	#endregion

	public override IntVector2 getGridPos(Vector2 worldPos) {
		worldPos -= gridMin;
		IntVector2 pos;
		pos.x = Mathf.RoundToInt(worldPos.x / size.x);
		pos.y = Mathf.RoundToInt(worldPos.y / size.y);
		return pos;
	}

	void Start() {
		var max = getGridPos(gridMax);
		gridObjects = new GridTile[max.x+1, max.y+1];
		foreach (var tile in tempTiles) {
			var p = getGridPos((Vector2)tile.transform.position);
			gridObjects[p.x, p.y] = tile;
			tile.gridPos = p;
		}
	}

	public static IntVector2 dirToVect(SquareGridDirection dir) {
		switch (dir) {
			case SquareGridDirection.East:
				return new IntVector2(1, 0);
			case SquareGridDirection.West:
				return new IntVector2(-1, 0);
			case SquareGridDirection.South:
				return new IntVector2(0, -1);
			case SquareGridDirection.North:
				return new IntVector2(0, 1);
			case SquareGridDirection.NorthEast:
				return new IntVector2(1, 1);
			case SquareGridDirection.NorthWest:
				return new IntVector2(-1, 1);
			case SquareGridDirection.SouthEast:
				return new IntVector2(1, -1);
			case SquareGridDirection.SouthWest:
				return new IntVector2(-1, -1);
			default:
				return new IntVector2(0, 0);
		}
	}
}