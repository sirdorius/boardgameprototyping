﻿using UnityEngine;
using TouchScript.Gestures.Simple;
using DG.Tweening;

public class SnapToGrid : MonoBehaviour {

	void OnEnable() {
		GetComponent<SimplePanGesture>().Panned += panHandler;
		GetComponent<TouchScript.Behaviors.Transformer2D>().enabled = false;
	}

	void OnDisable() {
		GetComponent<SimplePanGesture>().Panned -= panHandler;
		GetComponent<TouchScript.Behaviors.Transformer2D>().enabled = true;
	}

	private void panHandler(object sender, System.EventArgs e) {
		var p = Camera.main.ScreenToWorldPoint(((SimplePanGesture)sender).ScreenPosition);
		transform.DOMove(newPosition(p), 0.2f);
	}

	protected virtual Vector2 newPosition(Vector2 dropPos) {
		return SquareGrid.instance.getClosestPointOnGrid(dropPos);
	}
}