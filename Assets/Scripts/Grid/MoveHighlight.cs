﻿using UnityEngine;
using TouchScript.Gestures.Simple;
using System.Collections.Generic;

public class MoveHighlight : MonoBehaviour {

	#region Inspector
	public bool onlyFromInitialPosition = false;
	#endregion
	protected List<MovementDescriptor.ValidPosition> lastHighlights = new List<MovementDescriptor.ValidPosition>();

	void OnEnable() {
		if (onlyFromInitialPosition)
			GetComponent<SimplePanGesture>().PanStarted += pannedHandler;
		else
			GetComponent<SimplePanGesture>().Panned += pannedHandler;
		GetComponent<SimplePanGesture>().PanCompleted += panCompletedHandler;
	}

	void OnDisable() {
		if (onlyFromInitialPosition)
			GetComponent<SimplePanGesture>().PanStarted -= pannedHandler;
		else
			GetComponent<SimplePanGesture>().Panned -= pannedHandler;
		GetComponent<SimplePanGesture>().PanCompleted -= panCompletedHandler;
	}

	private void panCompletedHandler(object sender, System.EventArgs e) {
		foreach (var p in lastHighlights) {
			try {
				SquareGrid.instance.getTile(p.pos).highlight(0);
			}
			catch (System.NullReferenceException) {	}
		}
	}

	private void pannedHandler(object sender, System.EventArgs e) {
		panCompletedHandler(sender, e);
		if (SquareGrid.instance.isInsideGrid(transform.position)) {
			// inside grid, cancel last highlights and update new ones
			lastHighlights = GetComponent<MovementDescriptor>().validPositions(SquareGrid.instance.getGridPos(transform.position));
			foreach (var p in lastHighlights) {
				try {
					SquareGrid.instance.getTile(p.pos).highlight(p.type);
				}
				catch (System.NullReferenceException) { }
			}
		}
		else {
			// TODO outside of grid, highlight spawning positions
		}
	}

}