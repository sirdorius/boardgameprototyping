﻿using UnityEngine;
using System.Collections.Generic;

public class HexGridMovementDescriptor : MovementDescriptor {
	[System.Serializable]
	public struct HexMove {
		public HexGridDirection orientation;
		public int distance;
		public bool useOnlyLast;
		public GridTile.HighlightType type;
	}

	#region Inspector
	public List<HexMove> relativeMoves;
	#endregion

	public override List<ValidPosition> validPositions(IntVector2 current) {
		var moves = new List<ValidPosition>();
		foreach (var move in relativeMoves) {
			var t = current;
			for (int i = 0; i < move.distance; i++) {
				t += hexMoveToGridPos(move);
				if (!move.useOnlyLast || i == move.distance -1) {
					moves.Add(new ValidPosition { pos = t, type = move.type});
				}
			}
		}
		return moves;
	}

	IntVector2 hexMoveToGridPos(HexMove move) {
		switch (move.orientation) {
			case HexGridDirection.North:
				return new IntVector2(0, 1);
			case HexGridDirection.NorthEast:
				return new IntVector2(+1, 1);
			case HexGridDirection.NorthWest:
				return new IntVector2(-1, 0);
			case HexGridDirection.South:
				return new IntVector2(0, -1);
			case HexGridDirection.SouthEast:
				return new IntVector2(1, 0);
			case HexGridDirection.SouthWest:
				return new IntVector2(-1, -1);
			default:
				return new IntVector2(0, 0);
		}
	}
}