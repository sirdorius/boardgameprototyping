﻿using UnityEngine;

public abstract class Grid : SD.LevelSingleton<Grid> {
	#region Inspector
	public Vector2 size;
	#endregion

	protected GridTile[,] gridObjects;
	public abstract IntVector2 getGridPos(Vector2 worldPos);

	public Vector2 getClosestPointOnGrid(Vector2 worldPos) {
		return getWorldPos(getGridPos(worldPos)) ?? worldPos;
	}

	public bool isInsideGrid(Vector2 worldPos) {
		return getWorldPos(getGridPos(worldPos)) == null ? false : true;
	}

	public GridTile getTile(IntVector2 gridPos) {
		try {
			return gridObjects[gridPos.x, gridPos.y].GetComponent<GridTile>();
		}
		catch (System.IndexOutOfRangeException) {
			return null;
		}
	}

	public Vector2? getWorldPos(IntVector2 gridPos) {
		try {
			return getTile(gridPos).transform.position;
		}
		catch (System.NullReferenceException) {
			return null;
		}
	}

	#region Grid construction
	protected Vector2 gridMin = new Vector2(Mathf.Infinity, Mathf.Infinity);
	protected Vector2 gridMax = new Vector2(Mathf.NegativeInfinity, Mathf.NegativeInfinity);

	public abstract void registerGridTile(GridTile tile);
	protected void updateGridMinMax(GridTile tile, float x, float y) {
		if (x < gridMin.x)
			gridMin.x = x;
		if (y < gridMin.y)
			gridMin.y = y;

		if (x > gridMax.x)
			gridMax.x = x;
		if (y > gridMax.y)
			gridMax.y = y;
	}
	#endregion
}