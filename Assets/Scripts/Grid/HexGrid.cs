﻿using UnityEngine;
using System.Collections.Generic;

public enum HexGridDirection { North, NorthEast, NorthWest, South, SouthEast, SouthWest }

public class HexGrid : Grid {
	List<GridTile> tempTiles = new List<GridTile>();
	IntVector2 offset = new IntVector2(0,0);

	public override void registerGridTile(GridTile tile) {
		var p = getGridPos(tile.transform.position);
		tempTiles.Add(tile);
		updateGridMinMax(tile, p.x, p.y);
		tile.gridPos = p;
	}

	void Start() {
		offset = new IntVector2(-Mathf.RoundToInt(gridMin.x), -Mathf.RoundToInt(gridMin.y));
		gridObjects = new GridTile[
			Mathf.RoundToInt(gridMax.x - gridMin.x + 1),
			Mathf.RoundToInt(gridMax.y - gridMin.y + 1)
		];
		foreach (var t in tempTiles) {
			t.gridPos += offset;
			gridObjects[t.gridPos.x, t.gridPos.y] = t;
		}
	}

	public override IntVector2 getGridPos(Vector2 worldPos) {
		IntVector2 p;
		p.x = Mathf.RoundToInt(worldPos.x * (4 / 3f) / size.x);
		p.y = Mathf.RoundToInt((worldPos.x * (4 / 3f) / size.x + worldPos.y * 2 / size.y) / 2);
		return p + offset;
	}
}