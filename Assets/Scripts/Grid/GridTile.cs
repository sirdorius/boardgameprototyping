﻿using UnityEngine;


public class GridTile : MonoBehaviour {
	public enum HighlightType { None, Movement, Attack }
	public static Color[] colors = {Color.white, Color.cyan, Color.red};
	Color initColor;
	public IntVector2 gridPos {
		get {return _position;}
		set {
			_position = value;
			try {
				GetComponentInChildren<UnityEngine.UI.Text>().text = value.ToString();
			}
			catch (System.NullReferenceException) { }
		}
	}
	IntVector2 _position;

	public void Awake() {
		initColor = GetComponent<SpriteRenderer>().color;
		Grid.instance.registerGridTile(this);
	}

	public void highlight(HighlightType type) {
		GetComponent<SpriteRenderer>().color = (type != HighlightType.None ? colors[(int)type] : initColor);
	}
}