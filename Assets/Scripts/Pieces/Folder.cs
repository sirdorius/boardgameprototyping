﻿using UnityEngine;
using DG.Tweening;

namespace SD {
	public class Folder : Draggable {
		void OnTriggerStay2D(Collider2D coll) {
			Piece p = coll.GetComponent<Piece>();
			if (p && !p.isDragging && !isDragging) {
				p.transform.parent = transform;
				p.transform.DOLocalMove(Vector2.zero, 0.75f);
				p.transform.DOScale(Vector2.zero, 0.75f)
					.OnComplete(() => p.gameObject.SetActive(false));
			}
		}
	}
}