﻿using UnityEngine;
using UnityEngine.UI;
using SD.ObjectFade;

public class Reusable : MonoBehaviour {
	#region Inspector
	public bool showCount = true;
	public int initialCount = 1;
	#endregion

	#region Private
	public int count {
		get { return _count; }
		set {
			_count = value;
			try { 
				text.text = value.ToString();
			}
			catch (System.NullReferenceException) { }
		}
	}
	int _count;
	Text text;
	#endregion

	void Awake() {
		text = GetComponentInChildren<Text>();
		if (!showCount) {
			try {
				Destroy(GetComponentInChildren<Canvas>().gameObject);
			} catch (System.NullReferenceException) { }
		}
		count = initialCount;
	}

	void OnEnable() {
		GetComponent<TouchScript.Gestures.Simple.SimplePanGesture>().PanStarted += Reusable_PanStarted;
	}
	void OnDisable() {
		GetComponent<TouchScript.Gestures.Simple.SimplePanGesture>().PanStarted -= Reusable_PanStarted;
	}

	private void Reusable_PanStarted(object sender, System.EventArgs e) {
		var go = Instantiate(gameObject, transform.position, transform.rotation) as GameObject;
		try {
			Destroy(transform.Find("CountCanvas").gameObject);
		} catch (System.NullReferenceException) { }
		if (count <= 1)
			go.GetComponent<Reusable>().deactivateReusable();
		go.GetComponent<Reusable>().count = count - 1;
		Destroy(this);
	}

	private void deactivateReusable() {
		gameObject.fadeOutDestroy(0.3f);
	}
}