﻿using UnityEngine;
using TouchScript.Gestures.Simple;

namespace SD {
	public class Draggable : MonoBehaviour {
		public bool isDragging {
			get {
				return _isDragging;
			}
		}

		bool _isDragging = false;

		void OnEnable() {
			GetComponent<SimplePanGesture>().PanStarted += Piece_PanStarted;
			GetComponent<SimplePanGesture>().PanCompleted += Piece_PanCompleted;
		}

		void OnDisable() {
			GetComponent<SimplePanGesture>().PanStarted -= Piece_PanStarted;
			GetComponent<SimplePanGesture>().PanCompleted -= Piece_PanCompleted;
		}

		void Piece_PanCompleted(object sender, System.EventArgs e) {
			_isDragging = false;
		}

		void Piece_PanStarted(object sender, System.EventArgs e) {
			_isDragging = true;
		}
	}
}